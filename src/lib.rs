#![allow(unused_imports)]

extern crate serde;

use serde::{ser,de};

use std::result::Result as StdResult;
use std::{error,iter};

mod macros;

pub enum Borrow<'a, T: 'a>{
    Const(&'a T),
    Mut(&'a mut T),
}

pub struct Error<Q: Query>{
    pub query: Q,
    pub reason: ErrorReason,
}

pub enum ErrorReason{
    NoSuchField,
    RequiresMut,
}

pub type Result<Q: Query> = StdResult<Q::Output,Error<Q>>;

pub trait Query {
    type Output;
    const MUTABLE: bool;
    
    fn execute<R: Restable>(self,qeury: Borrow<R>) -> Self::Output;
}

pub struct Inquiry<'a,I,Q>
    where I: Iterator<Item = &'a str> + Clone,
          Q: Query,
{
    query: Q,
    path: iter::Peekable<I>,
}

impl<'a,I,Q> Inquiry<'a,I,Q>
    where I: Iterator<Item = &'a str> + Clone,
          Q: Query,
{
    pub fn new(path: I, query: Q) -> Self{
        Inquiry{
            query: query,
            path: path.peekable(),
        }
    }

    pub fn inquire<R: Restable>(mut self, next: &R) -> Result<Q>{
        if let Some(next_path) = self.path.next(){
            next.inquire(next_path,self)
        }else{
            if Q::MUTABLE {
                return Err(Error{
                    query: self.query,
                    reason: ErrorReason::RequiresMut,
                });
            }
            let borrow = Borrow::Const(next);
            Ok(self.query.execute(borrow))
        }
    }

    pub fn inquire_mut<R: Restable>(mut self, next: &mut R) -> Result<Q>{
        if let Some(next_path) = self.path.next(){
            next.inquire(next_path,self)
        }else{
            let borrow = Borrow::Mut(next);
            Ok(self.query.execute(borrow))
        }
    }
}

pub trait Restable: ser::Serialize + de::DeserializeOwned{

    fn inquire<'a,I,Q> (&self,path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
        where I: Iterator<Item = &'a str> + Clone,
              Q: Query;

    fn inquire_mut<'a,I,Q>(&mut self,path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
        where I: Iterator<Item = &'a str> + Clone,
              Q: Query;

}


