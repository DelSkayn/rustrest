use super::{Inquiry,Restable,Query,Error,ErrorReason,Result};

macro_rules! impl_primitive {
    ($ty:ident) => (
        impl Restable for $ty {
            fn inquire<'a,I,Q> (&self,_path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
                where I: Iterator<Item = &'a str> + Clone,
                      Q: Query
            {
                Err(Error{
                    query: request.query,
                    reason: ErrorReason::NoSuchField,
                })
            }

            fn inquire_mut<'a,I,Q>(&mut self,_path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
                where I: Iterator<Item = &'a str> + Clone,
                      Q: Query
            {
                Err(Error{
                    query: request.query,
                    reason: ErrorReason::NoSuchField,
                })
            }
        }    
    )
}

impl_primitive!(usize);
impl_primitive!(isize);
impl_primitive!(u8);
impl_primitive!(u16);
impl_primitive!(u32);
impl_primitive!(u64);
impl_primitive!(i8);
impl_primitive!(i16);
impl_primitive!(i32);
impl_primitive!(i64);
impl_primitive!(f32);
impl_primitive!(f64);
impl_primitive!(String);

#[macro_export]
macro_rules! impl_struct{
    ($ty:ident {$($field:ident),*} ) => (
        impl Restable for $ty {
            fn inquire<'a,I,Q> (&self,path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
                where I: Iterator<Item = &'a str> + Clone,
                      Q: Query
            {
                match path {
                    $(stringify!($field) => request.inquire(&self.$field),)*
                    _ => Err(Error{
                        query: request.query,
                        reason: ErrorReason::NoSuchField,
                    })
                }
            }

            fn inquire_mut<'a,I,Q>(&mut self,path: &str,request: Inquiry<'a,I,Q>) -> Result<Q>
                where I: Iterator<Item = &'a str> + Clone,
                      Q: Query
            {
                match path {
                    $(stringify!($field) => request.inquire_mut(&mut self.$field),)*
                    _ => Err(Error{
                        query: request.query,
                        reason: ErrorReason::NoSuchField,
                    })
                }
            }
        }    

    )
}

#[test]
mod test{

    use super::*;

    struct Foo{
        a: usize,
        b: u32,
    }

    impl_struct!(Foo{
        a,
        b,
    });
}
